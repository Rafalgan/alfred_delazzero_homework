package ponte;

public class Ponte {

	private boolean occupato=false;
	
	
public synchronized void occupa(){
	
	String name = Thread.currentThread().getName();
	
		while(occupato==true){
			System.out.println(name + " aspetta");
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		  occupato=true;
		  System.out.println(name + " occupa il ponte");
}
	
		
public synchronized void libera (){
	
		    occupato=false;		
		    String name = Thread.currentThread().getName();
			System.out.println(name + " libera il ponte");
			notifyAll();
		}
}