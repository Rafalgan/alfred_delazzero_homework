package ponte;

public class Macchina extends Thread {

	private int pos = 0;
	Ponte ponte;

	Macchina(Ponte ponte) {
		this.ponte = ponte;
	}

	public void run() {
		for (pos = 0; pos <= 100; pos += 10) {
			try {
				if (pos == 100)
					System.out.println(getName() + " arrivata");
				else
					System.out.println(getName() + " posizione= " + pos + "m");
				if (pos == 50)
					ponte.occupa();
				if (pos == 60)
					ponte.libera();
				sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}