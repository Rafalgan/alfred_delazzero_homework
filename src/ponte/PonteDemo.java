package ponte;

public class PonteDemo {

	public static void main(String[] args) {
		
		Ponte ponte = new Ponte();
		Macchina macchina  = new Macchina(ponte);
		Macchina macchina2 = new Macchina(ponte);
		Macchina macchina3 = new Macchina(ponte);
		
	    macchina.start();
	    macchina2.start();
	    macchina3.start();
		
	}

}