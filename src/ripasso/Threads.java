package ripasso;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Threads extends Thread{
	ObjectOutputStream outputStream;
	String s="msg";
	
	public void run() {
		super.run();
		System.out.println("thread " + getName() + " runs");
		boolean done = false;
		while(!done) {
			long time = 1000; // scrittura ogni time millisec
			try {
				sleep((long) (Math.random()*time));
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			try {
				synchronized (outputStream) {
					outputStream.writeObject(s);
					outputStream.flush();
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				done = true; //esce dal ciclo quando il client si disconnette
			}
		}
	}
}
