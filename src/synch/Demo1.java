package synch;

public class Demo1 {

	public static void main(String[] args) {
			Shared shared=new Shared();
			
			Printer printer=new Printer(shared);
			Printer printer2=new Printer(shared);
			Printer printer3=new Printer(shared);
			
			printer.start();
			printer2.start();
			printer3.start();

	}

}
