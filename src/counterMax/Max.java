package counterMax;

public class Max extends Thread {
	
	private int [] array;
	private int max;
	private int start;
	private int length;

	
	
	public Max(int[] array, int start, int length) {
		this.array = array;
		this.start = start;
		this.length = length;
	}


	@Override
	public void run() {
		
		max = array[0];
		
		for (int i = start; i < start + length; i++) {
			
			if(max < array[i]) max = array[i];
		}
		
		System.out.println("Max: " + max);
	}

}