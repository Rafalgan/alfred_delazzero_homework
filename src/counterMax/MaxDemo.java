package counterMax;

import java.util.Random;

public class MaxDemo {

	public static void main(String[] args) {
		
		int length = 100000000;
		int[] array = new int[length];
		

		Random random = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt();
		}
		
		Max max = new Max(array, 0, length/4);
		Max max2 = new Max(array, length/4, length/4);
		Max max3 = new Max(array, length/2, length/4);
		Max max4 = new Max(array, length*3/4, length/4);
		
		long time = System.currentTimeMillis();
		max.start();
		max2.start();
		max3.start();
		max4.start();
		
		try {
			max.join();
			max2.join();
			max3.join();
			max4.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long delta = System.currentTimeMillis() - time;
		System.out.println("Tempo impiegato: " + delta);

	}

}