package scuola_db;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JSplitPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI {
	private JTextField textNome;
	private JTextField textCog;
	private JTextField textData;
	private JTextField textComune;
	private JTextField nazione;
	private JTextField textInd;
	private JTextField textSez;
	private JTextField textAnno;
	
	public GUI(){

	JFrame f = new JFrame();
	f.getContentPane().setLayout(null);
	
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	tabbedPane.setBounds(10, 11, 414, 240);
	f.getContentPane().add(tabbedPane);
	
	JPanel panel = new JPanel();
	panel.setToolTipText("");
	tabbedPane.addTab("Alunno", null, panel, null);
	panel.setLayout(null);
	
	JLabel lblNome = new JLabel("Nome:");
	lblNome.setBounds(10, 11, 71, 28);
	panel.add(lblNome);
	
	JLabel lblCognome = new JLabel("Cognome:");
	lblCognome.setBounds(10, 48, 65, 28);
	panel.add(lblCognome);
	
	textNome = new JTextField();
	textNome.setBounds(92, 15, 141, 24);
	panel.add(textNome);
	textNome.setColumns(10);
	
	textCog = new JTextField();
	textCog.setColumns(10);
	textCog.setBounds(92, 50, 141, 24);
	panel.add(textCog);
	
	JLabel lblDataNascita = new JLabel("Data nascita:");
	lblDataNascita.setBounds(10, 91, 71, 14);
	panel.add(lblDataNascita);
	
	textData = new JTextField();
	textData.setBounds(92, 84, 86, 28);
	panel.add(textData);
	textData.setColumns(10);
	
	JLabel lblComuneNascita = new JLabel("Comune nascita:");
	lblComuneNascita.setBounds(10, 116, 86, 24);
	panel.add(lblComuneNascita);
	
	textComune = new JTextField();
	textComune.setBounds(92, 118, 141, 20);
	panel.add(textComune);
	textComune.setColumns(10);
	
	JLabel lblNazione = new JLabel("Nazione:");
	lblNazione.setBounds(10, 151, 46, 14);
	panel.add(lblNazione);
	
	nazione = new JTextField();
	nazione.setBounds(92, 148, 141, 20);
	panel.add(nazione);
	nazione.setColumns(10);
	
	JButton btnInviaA = new JButton("Invia");
	btnInviaA.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			//PERFORMED
		}
	});
	btnInviaA.setBounds(7, 178, 89, 23);
	panel.add(btnInviaA);
	
	JPanel panel_1 = new JPanel();
	tabbedPane.addTab("Classe", null, panel_1, null);
	panel_1.setLayout(null);
	
	JLabel lblIndirizzo = new JLabel("Indirizzo:");
	lblIndirizzo.setBounds(10, 11, 67, 21);
	panel_1.add(lblIndirizzo);
	
	JLabel lblSezione = new JLabel("Sezione:");
	lblSezione.setBounds(10, 43, 46, 14);
	panel_1.add(lblSezione);
	
	JLabel lblAnno = new JLabel("Anno:");
	lblAnno.setBounds(10, 68, 46, 14);
	panel_1.add(lblAnno);
	
	textInd = new JTextField();
	textInd.setBounds(87, 11, 86, 20);
	panel_1.add(textInd);
	textInd.setColumns(10);
	
	textSez = new JTextField();
	textSez.setBounds(87, 40, 86, 20);
	panel_1.add(textSez);
	textSez.setColumns(10);
	
	textAnno = new JTextField();
	textAnno.setBounds(87, 65, 36, 20);
	panel_1.add(textAnno);
	textAnno.setColumns(10);
	
	JButton btnInviaC = new JButton("Invia");
	btnInviaC.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			//performed classe
		}
	});
	btnInviaC.setBounds(10, 93, 89, 23);
	panel_1.add(btnInviaC);
	
	JPanel panel_2 = new JPanel();
	tabbedPane.addTab("New tab", null, panel_2, null);
	panel_2.setLayout(null);
	
	}
}
