package scuola_db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class EQuery {
	 static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 static final String DB_URL = "jdbc:mysql://localhost/scuola";
	

	 //  Database credentials
	 static final String USER = "root";
	 static final String PASS = "";
	 
	 public static void main(String[] args) {
	 Connection conn = null;
	 Statement stmt = null;
	 String indirizzo;
	 char sez;
	 char anno;
	 
	 try{
	    //STEP 2: Register JDBC driver
	  // Class.forName("com.mysql.jdbc.Driver"); comando per caricare una classe in memoria passando il nome
	   

	    //STEP 3: Open a connection
	    System.out.println("Connecting to database...");
	    conn = DriverManager.getConnection(DB_URL,USER,PASS); //connettersi al database con le mie credenziali

	    //STEP 4: Execute a query
	    System.out.println("Creating statement...");
	    stmt = conn.createStatement();
	    
	    String sql;
	    
	    indirizzo="informatica";
		sez='C';
		anno='2';
		sql = "INSERT INTO classe (PK_Classe, anno, sezione, indirizzo) VALUES ("+null+", '"+anno+"','"+sez+"','"+indirizzo+"');";
	    stmt.execute(sql);
	    
	    stmt.close();
	    conn.close();
	 }catch(SQLException se){
	    //Handle errors for JDBC
	    se.printStackTrace();
	 }catch(Exception e){
	    //Handle errors for Class.forName
	    e.printStackTrace();
	 }finally{
	    //finally block used to close resources
	    try{
	       if(stmt!=null)
	          stmt.close();
	    }catch(SQLException se2){
	    }// nothing we can do
	    try{
	       if(conn!=null)
	          conn.close();
	    }catch(SQLException se){
	       se.printStackTrace();
	    }//end finally try
	 }//end try
	 System.out.println("Goodbye!");
	}//end main
}
