package Semaforo;

class Automobile extends Thread {
    Semmaforo s;
    static int posizione = 0;
    private long t1;

    Automobile(Semmaforo s) {
        this.s = s;
    }

    @Override
    public void run() {
        super.run();
        t1 = System.currentTimeMillis();
        while (posizione != 100) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            posizione += 10;
            System.out.println("Posizione= " + posizione);
            if(posizione==50) {
                s.attendiVerde();
            }
            if (posizione == 100)
                System.out.println("Tempo impiegato " + (System.currentTimeMillis() - t1) + "; posizione " + posizione);
        }
    }
}