package chat;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;


public class Writer extends Thread{

    private Socket clientSocket;
    private boolean done=false;
    private String s;
    private ObjectOutputStream output;
	private ObjectInputStream input;
	List<Writer> t;
	
    public Writer(Socket clientSocket, List<Writer> t) {
        this.clientSocket = clientSocket;
        this.t=t;
    }

    public void run() {
        try {
        	output=new ObjectOutputStream(clientSocket.getOutputStream());
        	output.flush();
			input=new ObjectInputStream(clientSocket.getInputStream());
			s=(String)input.readObject();
            
            while(done=false){
            	
            	try {
    				synchronized (output) {
    					
    					
    					s=(String) input.readObject();
    					for(int i=1;i<=t.size();i++){
    						if(t.get(i)!=this)
    						t.get(i).output.writeObject(s);
    						t.get(i).output.flush();
    					}
    					//output.writeObject(s);
    					//output.flush();
    				
    				}
    				
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            	
            }
            
            
            
            output.close();
            input.close();
            System.out.println("Request processed: ");
        } catch (IOException e) {
            //report exception somewhere.
            e.printStackTrace();
        } catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
}
