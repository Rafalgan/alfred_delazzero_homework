package indovina_numero;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ObjectOutputStream output2;
	private ObjectInputStream input2;
	private ServerSocket server;
	private Socket connection;
	private Socket connection2;
	
	private int port= 32041;
	private int tent=1;
	private String nr;
	
public Server(){
	
	try{
		server= new ServerSocket(port);
		String message="server in attesa sulla porta"+port;
		System.out.println(message);
		connection=server.accept();
		connection2=server.accept();
		
		message =  "Connesso con " + connection.getInetAddress().getHostName();
		System.out.println(message);
		message =  "Connesso con " + connection2.getInetAddress().getHostName();
		System.out.println(message);
		
		output=new ObjectOutputStream(connection.getOutputStream());
		input=new ObjectInputStream(connection.getInputStream());
		output2=new ObjectOutputStream(connection2.getOutputStream());
		input2=new ObjectInputStream(connection2.getInputStream());
		
		System.out.println("Stream ottenuti");
		
		//comunicazione
		
		String n1 = (String) input.readObject();
		String n2= (String) input2.readObject();
		
		
		
		do{
			if(tent!=6){
			System.out.println("tent= "+tent);
			if(tent%2!=0){
				System.out.println("Tentativo a client1");
				output.writeObject("GET");
				output.flush();
				System.out.println("GET client1");
				nr=(String) input.readObject();
				tent++;
			}
			if(nr==n2){
				output.writeObject("WIN");
				output.flush();
				tent=6;
				}
			}
			
			if(tent!=6){
			
				if(tent%2==0){
				System.out.println("Tentativo a client2");
				output2.writeObject("GET");
				output2.flush();
				System.out.println("GET client2");
				nr=(String) input.readObject();
				tent++;				
			}
				System.out.println("tent= "+tent);
			if(nr==n1){
				output2.writeObject("WIN");
				output2.flush();
				tent=6;
				}
			}
			System.out.println("tent= "+tent);
		}while(tent<6);
		
		
	}
	catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	finally {
		// Step 5: chiusura della connessione
		System.out.println("chiusura connessione");
		try {
			connection.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			if(output != null) output.close();
		}
		catch( IOException ioException ) {
			ioException.printStackTrace();
		}   


		try {
			if(input != null)input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
}
	public static void main(String[] args){
		new Server();
	}
}
