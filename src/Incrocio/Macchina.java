package Incrocio;

class Macchina extends Thread{
    int dist=0,strada;
    int x,y;
    Incrocio i;

    Macchina(Incrocio i, int s){
        this.i=i;
        strada=s;
        i.occupa(strada);
        if(strada==1){
            x=190;
            y=10;
        }
        else if(strada==2){
            x=410;
            y=200;
        }
        else if(strada==3){
            x=220;
            y=410;
        }
        else{
            x=0;
            y=230;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    @Override
    public void run() {
        super.run();
        while(dist!=40){
            try{
                Thread.sleep(100);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            dist+=1;
            System.out.println("La macchina "+Thread.currentThread().getName()+" = "+dist);
            if(dist==18){
                try {
                    //i.occupa(strada);
                    i.checkRight(strada);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(dist==22){
                System.out.println("La macchina "+Thread.currentThread().getName()+" e' uscita dall'incrocio.");
                i.libera(strada);
            }
            if(strada==1){
                y+=10;
            }
            else if(strada==2){
                x-=10;
            }
            else if(strada==3){
                y-=10;
            }
            else{
                x+=10;
            }
        }
        System.out.print(Thread.currentThread().getName()+" arrivata.");
    }
}
