package teatro;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket connection;
	private int port = 32041,j=0,i=0,n;
	boolean[] posti=new boolean[15];
	String[] nome=new String[20];
	String[] cognome=new String[20];
	
	public Server(){
		//inizializzo tutti i posti a libero.
		for(int i=0;i<15;i++)
			posti[i]=true;
		try {
			server=new ServerSocket(port);
			String message="server in attesa sulla porta"+port;
			System.out.println(message);
			while(true){
				
			
			connection=server.accept();
			
			message =  "Connesso con " + connection.getInetAddress().getHostName();
			System.out.println(message);
			
			output=new ObjectOutputStream(connection.getOutputStream());
			input=new ObjectInputStream(connection.getInputStream());
			
			output.flush();
			
			nome[j]= (String) input.readObject();
			cognome[j]=(String) input.readObject();
			j++;
			
			n=(int) input.readObject();
			switch(n){
			case 1:	output.writeObject(posti);
					output.flush();
					System.out.println("Invio posti liberi.");
					break;
			case 2:
					break;
			case 3:
					break;
			case 4:
					break;
			case 5:
					break;
			default: break;
				}
			
			
			}
			
			
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			// Step 5: chiusura della connessione
			System.out.println("chiusura connessione");
			try {
				connection.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output != null) output.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input != null)input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		
	}
	
	public static void main(String[] args){
		new Server();
	}
}
