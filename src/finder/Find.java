package finder;

public class Find extends Thread {

	private int[] array;
	private int find;
	private int start;
	private int length;
	private int position=-1;
	private Shared s;

	public Find(int[] array, int start, int length, int find, Shared s) {
		this.array = array;
		this.start = start;
		this.length = length;
		this.find = find;
		this.s = s;
	}

	@Override
	public void run() {

		for (int i = start; i < start + length; i++) {

			if (s.isFind())
				break;
			if (find == array[i]){
				this.setPosition(i);
				s.Found();
				break;
			}
		}
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}