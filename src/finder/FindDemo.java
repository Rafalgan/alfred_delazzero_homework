package finder;

import java.util.Random;

public class FindDemo {
	public static void main(String[] args) {
		
		int n = 32;
		int length = 100000000;
		int[] array = new int[length];
		
		Random random = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt();			
		}
		//array[50]=32;
		Shared s = new Shared();
		
		Find find = new Find(array, 0, length/2, n, s);
		Find find2 = new Find(array, length/2, length/2, n, s);
		
		long time = System.currentTimeMillis();
		find.start();
		find2.start();
		
		try {
			find.join();
			find2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (find.getPosition()==-1&&find2.getPosition()==-1)
			System.out.println("Numero non trovato!");
		else if (find.getPosition() != -1||find2.getPosition()!=-1)
			System.out.print("Numero trovato in una posizione");
		
		long delta = System.currentTimeMillis() - time;
		System.out.println("Tempo impiegato: " + delta);
		
	}
}